# README #

### What is SearchManager? ###
**SearchManager** is a lightweight utility that **provides a robust,flexible interface and mechanism to search, query and filter structured data as exposed by a JPA compliant ORM framework like Hibernate or Spring Data DIRECTLY from a (dynamically provided) string-based query** that may, for example have been received to a RESTful endpoint from a standard web or mobile client HTTP request. At its core is a custom search syntax specification and associated parser which allows for arbitrarily complex queries to be constructed from realatively simple string token primitives. The returned Search instance may then be modified as required before be passed into the provided **compileSearch** method which transforms the Search into a standards compliant **JPA Data Specification**,which, in turn, may be passed directly into any JPA Data Repository to Search/Filter the associated domain data as required.

![Scheme](search-manager-diagram-trans.png)

### Getting Setup ###
SearchManager uses the **JitPack** platform as a build and binary distribution agent. What follows are instructions on integrating 
SearchManager into a **Maven** based project. See the [jitpack homepage ](https://www.jitpack.io/) for details on configuring the
utility to work with alternative build tools (i.e Gradle etc). That said, for Maven based projects, the setup process essentially 
involves referecing JitPack and the SearchManager utility in the projects **POM** file:

* **Add reference to JitPack as a Repository:**
```
 <repositories>
        
        <repository>
            <id>jitpack.io</id>
            <url>https://jitpack.io</url>
        </repository>

 </repositories>
```

* **Add SearchManager as a Dependency:**
```
 <dependencies>

        <dependency>
            <groupId>org.bitbucket.thompsonlabs</groupId>
            <artifactId>uk.thompsonlabs.utilities.data.search</artifactId>
            <version>fff4d25917</version>
        </dependency>

  </dependencies>
```

Thats it, simply reload the POM file and rebuild your project as necessary to complete the setup process.


### SearchManager Usage ###

**Prepare Project Repository Classes.**    
Ensure **all** project Repository interfaces, associated with the domain data you would like to query/filter on are updated to extend the **JpaSpecificationExecutor** Interface. A fully defined Repository interface may looks as follows (applicable inports have been omitted for brevity): 

```Java
@Repository
public interface PartnerRepository extends JpaRepository<Partner,String>, JpaSpecificationExecutor<Partner> {
}
```
<br>    

**Instansiate the SearchManager utility.**    
Grab reference to a SearchManager instance via either of the two provided factory methods: `newInst()` or `getSingleInstance()`  SearchManager is inherently thread-safe and thus the **singleton** variant
should be used where possible:
```Java
final var searchManager = SearchManager.getSingletonInstance()
```
<br>    

**Review the SearchManager query langauge primitives.**   
What follows is a breakdown of the SearchManager query primitives and their respective functions:

  | Syntax         |            Description            |        Usage       |                   Usage Explained                   |
  |----------------|:---------------------------------:|:------------------:|:---------------------------------------------------:|
  |      **:**     | For **EQUALITY** comparison       |     name:giles     |                name **equals** giles                |
  |      **!**     | For **NEGATION** comparison       |     name!giles     |             name **doesn't equal** giles            |
  |      **>**     | For **GREATER THAN** comparison   |       age>30       |               age **greater than** 30               |
  |      **<**     | For **LESS THAN** comparison      |       age<50       |                 age **less than** 50                |
  |   **~[VAL]***  | For **STARTS WITH** comparison    |      name~gil*     |               name **starts with** gil              |
  |   **~*[VAL]**  | For **ENDS WITH** comparison      |      name~*les     |                name **ends with** les               |
  | **~\*[VAL]\*** | For **CONTAINS** comparison       |    name~\*ile\*    |                name **contains** ile                |
  |      **'**     | For **CONJUNCTIVE OR** comparison | name:giles,'age<50 | name **equals** giles **or** age **less than** 50   |

<br>    

**Build and execute search query**    
First combine multiple syntax primitives,as defined above,to construct a complex search query. The
query is then passed into the SearchManager's `parseSearch` method which will parse the provided 
search string to a **Search** instance.
```Java
//build search query string
final var searchQueryStr = "lastName:thompson,age>25,daysSinceLastSignIn<2,'lastName!thompson,email~*hotmail*"

//parse search query string to Search instance.
final var search = searchManager.parseSearch(searchQueryStr);
```
Next pass the Search instance to the SearchManager's `compileSearch` method which compile the Search instance to a **JPA Specification** 
```Java
//compile search to JPA Specification
final var specification = searchManager.compileSearch(search);
```
Finally pass the returned Specification instance into a target Repository to apply the search to the associated domain data and obtain the results; as intimated above, **any** standard JPA Repository may be used providing it implements the `JpaSpecificationExecutor` interface. Here we utilise the **PartnerRepository** defined in the example above:
```Java
//find all entries that meet our specification
final var matchingEntries = partnerRepository.findAll(specification);
```



### License ###
* MIT

### Contacts ###

* Name: Giles Thompson
* Email: giles@thompsonlabs.uk