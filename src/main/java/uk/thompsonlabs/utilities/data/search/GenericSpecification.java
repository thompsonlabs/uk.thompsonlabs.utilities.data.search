package uk.thompsonlabs.utilities.data.search;

import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;


public final class GenericSpecification<T> implements Specification<T> {

    private final SearchCriteria searchCriteria;

    public GenericSpecification(SearchCriteria searchCriteria) {
        this.searchCriteria = searchCriteria;
    }

    public static GenericSpecification newInst(SearchCriteria searchCriteria){

        return new GenericSpecification(searchCriteria);
    }


    @Override
    public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {

        switch (searchCriteria.getSearchOperation()) {
            case EQUALITY:
                if(searchCriteria.getValue().equalsIgnoreCase("true") || searchCriteria.getValue().equalsIgnoreCase("false")){
                    return criteriaBuilder.equal(root.get(searchCriteria.getKey()), Boolean.valueOf(searchCriteria.getValue()));
                }
                /**
                if(isNumeric(searchCriteria.getValue())){

                    final var decimalFormatObj = new DecimalFormat();
                    decimalFormatObj.setParseBigDecimal(true);

                    final BigDecimal parsedBigDecimalValue = (BigDecimal) decimalFormatObj.parse(searchCriteria.getValue(),new ParsePosition(0));

                    return criteriaBuilder.equal(root.get(searchCriteria.getKey()).as(BigDecimal.class),parsedBigDecimalValue);
                }
                 */
                return criteriaBuilder.equal(root.get(searchCriteria.getKey()), searchCriteria.getValue());
            case NEGATION:
                if(searchCriteria.getValue().equalsIgnoreCase("true") || searchCriteria.getValue().equalsIgnoreCase("false")){
                    return criteriaBuilder.equal(root.get(searchCriteria.getKey()), Boolean.valueOf(searchCriteria.getValue()));
                }
                return criteriaBuilder.notEqual(root.get(searchCriteria.getKey()), searchCriteria.getValue());
            case GREATER_THAN:
                return criteriaBuilder.greaterThan(root.<String>get(
                        searchCriteria.getKey()), searchCriteria.getValue().toString());
            case LESS_THAN:
                return criteriaBuilder.lessThan(root.get(
                        searchCriteria.getKey()), searchCriteria.getValue().toString());
            case LIKE:
                return criteriaBuilder.like(root.get(
                        searchCriteria.getKey()), searchCriteria.getValue().toString());
            case STARTS_WITH:
                return criteriaBuilder.like(root.get(searchCriteria.getKey()), searchCriteria.getValue() + "%");
            case ENDS_WITH:
                return criteriaBuilder.like(root.get(searchCriteria.getKey()), "%" + searchCriteria.getValue());
            case CONTAINS:
                return criteriaBuilder.like(root.get(
                        searchCriteria.getKey()), "%" + searchCriteria.getValue() + "%");
            default:
                return null;
        }
    }

    private static boolean isNumeric(String str) {
        return str.matches("-?\\d+(\\.\\d+)?");  //match a number with optional '-' and decimal.
    }
}

