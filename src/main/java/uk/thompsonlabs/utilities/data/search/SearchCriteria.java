package uk.thompsonlabs.utilities.data.search;

public final class SearchCriteria {

    private final String key;

    private final SearchOp searchOperation;

    private final String value;

    private final boolean orCriteria;

    private SearchCriteria(String key, SearchOp searchOperation, String value,boolean orCriteria) {
        this.key = key;
        this.searchOperation = searchOperation;
        this.value = value;
        this.orCriteria = orCriteria;
    }

    public static SearchCriteria newInst(String key,SearchOp operation,String value,boolean orCriteria){

        return new SearchCriteria(key,operation,value,orCriteria);
    }

    public String getKey() {
        return key;
    }

    public SearchOp getSearchOperation() {
        return searchOperation;
    }

    public String getValue() {
        return value;
    }

    public boolean isOrCriteria(){

        return orCriteria;
    }
}
