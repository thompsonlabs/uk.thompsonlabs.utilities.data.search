package uk.thompsonlabs.utilities.data.search;

import java.util.ArrayList;
import java.util.List;

public final class Search {

    private final List<SearchCriteria> searchCriteriaList;

    private Search() {

        searchCriteriaList = new ArrayList<>();
    }

    private void addSearchCriteria(String key,SearchOp operation,String value,boolean orCriteria){

        this.searchCriteriaList.add(SearchCriteria.newInst(key, operation, value,orCriteria));
    }

    /**
     *  public interface method that may be used to manually add additional criteria to this search
     *  (i.e post the SearchManagers parsing of the query string to an instance of this Search class)
     * @param key The field name.
     * @param operation The operation to apply.
     * @param value The value to associate with the field.
     * @param orCriteria A boolean flag which denotes whether or not this an OR criteria.
     * @return Search Reference to THIS search instance to allow multiple calls to this method to be chained.
     **/
    public Search addAdditionalSearchCriteria(String key,SearchOp operation,String value,boolean orCriteria){

        this.addSearchCriteria(key, operation, value, orCriteria);
        return this;
    }

    public int getSearchCriteriaCount(){

        return this.searchCriteriaList.size();
    }

    public SearchCriteria getSearchCriteria(int index){

        return this.searchCriteriaList.get(index);
    }

    public List<SearchCriteria> getAllSearchCriteria(){

        return List.of(this.searchCriteriaList.toArray(new SearchCriteria[]{}));
    }

    public static SearchBuilder Builder(){

        return new SearchBuilder();
    }


    public static class SearchBuilder{

        final Search search;

        public SearchBuilder() {

            this.search = new Search();
        }

        public SearchBuilder withCriteria(String key,SearchOp operation,String value,boolean orCriteria){

            if(orCriteria && this.search.getSearchCriteriaCount() == 0)
                throw new IllegalArgumentException("Invalid OR criteria: key: "+key+ " value: "+value+". OR criteria CANNOT be the first argument in a search.");

            this.search.addSearchCriteria(key, operation, value,orCriteria);

            return this;
        }

        public Search build(){

            return this.search;
        }

    }
}
