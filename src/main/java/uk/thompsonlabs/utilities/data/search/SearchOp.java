package uk.thompsonlabs.utilities.data.search;

public enum SearchOp {

    EQUALITY, NEGATION, GREATER_THAN, LESS_THAN, LIKE,STARTS_WITH, ENDS_WITH, CONTAINS;

    public static SearchOp getSimpleOperation(char input) {
        switch (input) {
            case ':':
                return EQUALITY;
            case '!':
                return NEGATION;
            case '>':
                return GREATER_THAN;
            case '<':
                return LESS_THAN;
            case '~':
                return LIKE;
            default:
                return null;
        }
    }

}
