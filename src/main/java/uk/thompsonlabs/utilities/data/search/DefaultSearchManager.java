package uk.thompsonlabs.utilities.data.search;

import org.springframework.data.jpa.domain.Specification;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/** Default implementation of a SearchManager. */
public class DefaultSearchManager implements SearchManager {
    @Override
    public Search parseSearch(String queryString) {

        Pattern pattern = Pattern.compile("(\\p{Punct}?)(\\w+?)(:|<|>|!|~)(\\p{Punct}?)(\\w+?)(\\p{Punct}?),");

        Matcher matcher = pattern.matcher( queryString + ",");

        var searchBuilder = Search.Builder();

        while (matcher.find()) {

            //like is a SPECIAL case it will break down to START WITH,CONTAINS OR ENDS WITH.
            char opChar = matcher.group(3).charAt(0);

            if(opChar == '~'){

                SearchOp searchOp;

                if(!matcher.group(4).isEmpty() && matcher.group(4).charAt(0) == '*'){

                    searchOp = (!matcher.group(6).isEmpty() && matcher.group(6).charAt(0) == '*') ? SearchOp.CONTAINS : SearchOp.ENDS_WITH;
                }
                else if(!matcher.group(6).isEmpty() && matcher.group(6).charAt(0) == '*'){

                    searchOp = SearchOp.STARTS_WITH;
                }
                else{
                    throw new IllegalArgumentException("An Illegal \"LIKE\" criteria was specified.");
                }

                searchBuilder.withCriteria(matcher.group(2),
                        searchOp,
                        matcher.group(5),
                        !matcher.group(1).isEmpty() && matcher.group(1).charAt(0) == '\'');

            }

            else {
                searchBuilder.withCriteria(matcher.group(2),
                        SearchOp.getSimpleOperation(opChar),
                        matcher.group(5),
                        !matcher.group(1).isEmpty() && matcher.group(1).charAt(0) == '\'');
            }

        }


        Search builtSearch = searchBuilder.build();

        return builtSearch;
    }

    @Override
    public Specification compileSearch(Search search) {

        if(search == null || search.getSearchCriteriaCount() <= 0){
            return null;
        }

        Specification fullSpec = null;

        var allSearchCriteria = search.getAllSearchCriteria();

        for(SearchCriteria criteria : allSearchCriteria){

            if(fullSpec == null){

                //First specification is guarenteed NOT to be an OR criteria.
                fullSpec = Specification.where(GenericSpecification.newInst(criteria));
            }
            else{

                if(criteria.isOrCriteria()){

                    fullSpec = fullSpec.or(GenericSpecification.newInst(criteria));
                }
                else{

                   fullSpec =  fullSpec.and(GenericSpecification.newInst(criteria));
                }

            }

        }

        return fullSpec;
    }
}

