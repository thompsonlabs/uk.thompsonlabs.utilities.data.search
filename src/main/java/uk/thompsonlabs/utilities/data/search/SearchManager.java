package uk.thompsonlabs.utilities.data.search;

import org.springframework.data.jpa.domain.Specification;

public interface SearchManager {

    static SearchManager singletonSearchManagerInstance = new DefaultSearchManager();

    static SearchManager newInst(){

        return new DefaultSearchManager();
    }

    static SearchManager getSingletonInstance(){

        return singletonSearchManagerInstance;
    }

    Search parseSearch(String queryString);

    Specification compileSearch(Search search);
}
